# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased • •

## 0.2.1 • 2024.03.21 

### Added
- added Tethys processor labels to Dockerfile

### Changed
- updated base runner image to UBI 8.8
- change ci/cd docker image name to full path
- updated rust from 1.73.0 -> 1.76.0

## 0.2.0 • 2023.05.10

### Added
- updated Dockerfile to use cargo chef

## 0.1.0 • 2023.04.13

Initial release

### Added
- send to unicast or multicast addresses
- supports both IPv4 and IPv6
- Tethys processor interface
    - supports unary and bidirectional gRPC connections
