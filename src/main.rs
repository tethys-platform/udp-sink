use local_ip_address::list_afinet_netifas;
use socket2::{Domain, Protocol, SockAddr, Socket, Type};
use std::net::{IpAddr, Ipv4Addr, SocketAddr, ToSocketAddrs};
use tokio::net::UdpSocket as TokioUdpSocket;
use tokio::sync::mpsc;
use tokio_stream::{wrappers::ReceiverStream, StreamExt};
use tonic::{transport::Server, Response, Status};

use tethys::process_data_response::ProcessResult;
use tethys::processor_server::{Processor, ProcessorServer};
use tethys::ProcessDataResponse;

#[macro_use]
extern crate log;

pub mod tethys {
    tonic::include_proto!("tethys"); // The string specified here must match the proto package name
}

mod config;

struct UdpSinkServer {
    config: config::Config,
    remote_addr: SocketAddr,
    udp_socket: TokioUdpSocket,
}

#[tonic::async_trait]
impl Processor for UdpSinkServer {
    type ProcessDataStreamStream = ReceiverStream<Result<tethys::ProcessDataResponse, Status>>;

    async fn process_data(
        &self,
        req: tonic::Request<tethys::ProcessDataRequest>,
    ) -> Result<Response<tethys::ProcessDataResponse>, Status> {
        let msg = req.into_inner();

        debug!("[unary] Received: {:?}", msg.content);

        if self.config.mtu_size != 0 {
            let msg = &msg.content[..];

            for chunk in msg.chunks(self.config.mtu_size) {
                self.udp_socket
                    .send_to(chunk, self.remote_addr)
                    .await
                    .expect("Failed to send message");
                }

        } else {
            self.udp_socket
                .send_to(msg.content.as_ref(), self.remote_addr)
                .await
                .expect("Failed to send message");
        }

        debug!("[unary] Sent message to {:?}", self.remote_addr);

        let resp = ProcessDataResponse {
            request_id: msg.id,
            result: ProcessResult::Drop as i32,
            ..Default::default()
        };

        Ok(Response::new(resp))
    }

    async fn process_data_stream(
        &self,
        req: tonic::Request<tonic::Streaming<tethys::ProcessDataRequest>>,
    ) -> Result<tonic::Response<Self::ProcessDataStreamStream>, tonic::Status> {
        let mut in_stream = req.into_inner();
        let (tx, rx) = mpsc::channel(128);

        let udp_socket = create_udp_socket(self.remote_addr, self.config.clone());

        let remote_addr = self.remote_addr;
        let config = self.config.clone();

        tokio::spawn(async move {
            while let Some(result) = in_stream.next().await {
                match result {
                    Ok(msg) => {
                        debug!("[stream] Received: {:?}", msg.content);

                        if config.mtu_size != 0 {
                            let msg = &msg.content[..];

                            for chunk in msg.chunks(config.mtu_size) {
                                udp_socket
                                    .send_to(chunk, remote_addr)
                                    .await
                                    .expect("Failed to send message");
                            }

                        } else {
                            udp_socket
                                .send_to(msg.content.as_ref(), remote_addr)
                                .await
                                .expect("Failed to send message");
                        }

                        debug!("[stream] Sent message to {:?}", remote_addr);

                        let resp = ProcessDataResponse {
                            result: ProcessResult::Drop as i32,
                            request_id: msg.id,
                            ..Default::default()
                        };

                        tx.send(Ok(resp))
                            .await
                            .expect("Failed to send to ReceiverStream");
                    }
                    Err(err) => println!("error -- {err:?}"),
                }
            }
        });

        // stream responses back
        Ok(Response::new(ReceiverStream::new(rx)))
    }
}

fn create_udp_socket(remote_addr: SocketAddr, config: config::Config) -> TokioUdpSocket {
    let domain = if remote_addr.is_ipv6() {
        Domain::IPV6
    } else {
        Domain::IPV4
    };

    let socket_addr = if remote_addr.is_ipv6() {
        info!("Binding socket as IPv6");
        SocketAddr::new(IpAddr::from([0, 0, 0, 0, 0, 0, 0, 1]), 0u16)
    } else {
        info!("Binding socket as IPv4");
        SocketAddr::new(IpAddr::from([0, 0, 0, 0]), 0u16)
    };

    let socket =
        Socket::new(domain, Type::DGRAM, Some(Protocol::UDP)).expect("Failed to create socket");

    socket
        .set_reuse_address(true)
        .expect("Failed to set SO_REUSEADDR");

    if let Err(err) = socket.set_send_buffer_size(config.socket_send_buffer_size) {
        warn!("Failed to set send buffer: {err:?}");
    }

    // Join multicast group if remote address is a multicast address
    if remote_addr.ip().is_multicast() {
        info!("Identified address as multicast");

        match remote_addr.ip() {
            IpAddr::V4(_ip4) => {
                let local_address: Ipv4Addr = config
                    .local_address
                    .parse()
                    .expect("local_address parse error");

                let interface = get_local_address(config.local_network_interface, local_address);
                socket
                    .set_multicast_if_v4(&interface)
                    .expect("Failed to set IP_MULTICAST_IF option");
            }
            IpAddr::V6(_ip6) => {
                let interface = get_interface_index(config.local_network_interface);

                socket
                    .set_multicast_if_v6(interface)
                    .expect("Failed to set IP_MULTICAST_IF option");
            }
        }
    }

    socket
        .bind(&SockAddr::from(socket_addr))
        .expect("Failed to bind UDP socket");

    // Create Tokio UDP socket
    let udp_socket =
        TokioUdpSocket::from_std(socket.into()).expect("Failed to create Tokio UDP socket");
    debug!("Created UDP socket: {udp_socket:?}");

    udp_socket
}

// if a network interface was provided, then try to get the address for it
// if an interface wasn't provided or anything goes wrong, default back to
// the configured local address
fn get_local_address(interface: Option<String>, address: Ipv4Addr) -> Ipv4Addr {
    if let Some(interface) = interface {
        let ifas = list_afinet_netifas();

        if ifas.is_err() {
            warn!("Error getting network interfaces list: {:?}", ifas.err());
            return address;
        }

        let ifas = ifas.unwrap();

        let found_interface = ifas
            .iter()
            .find(|(name, ipaddr)| *name == interface.as_str() && matches!(ipaddr, IpAddr::V4(_)));

        match found_interface {
            Some((name, ipaddr)) => {
                if let IpAddr::V4(ipaddr) = ipaddr {
                    info!("Using the local address for {name}: {ipaddr:?}");
                    return *ipaddr;
                }
            }
            None => {
                error!("Couldn't find a network interface named {interface}");
                return address;
            }
        }
    }

    address
}

fn get_interface_index(interface: Option<String>) -> u32 {
    if let Some(interface) = interface {
        let ifas = list_afinet_netifas();

        if ifas.is_err() {
            warn!("Error getting network interfaces list: {:?}", ifas.err());
            return 0;
        }

        let ifas = ifas.unwrap();

        if let Some(index) = ifas
            .iter()
            .position(|(name, _)| *name == interface.as_str())
        {
            index as u32
        } else {
            0
        }
    } else {
        0
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = config::load_config();

    let env = env_logger::Env::default()
        .filter_or(
            "LOG_LEVEL",
            if config.debug {
                "udp_sink=DEBUG"
            } else {
                "INFO"
            },
        )
        .write_style_or("LOG_COLORS", "never");
    env_logger::Builder::from_env(env).init();

    debug!("Running in DEBUG mode");

    let host_and_port = format!("{}:{}", config.hostname, config.host_port);

    let remote_addr = host_and_port
        .to_socket_addrs()
        .expect("Failed to parse address from hostname")
        .next()
        .unwrap();

    // create a socket to be used for unary requests
    let udp_socket = create_udp_socket(remote_addr, config.clone());

    let server = UdpSinkServer {
        config: config.clone(),
        remote_addr,
        udp_socket,
    };

    let addr = format!("0.0.0.0:{}", config.port).parse().unwrap();
    info!("Starting udp-sink @ {}", addr);
    info!("Config: {config:?}");

    Server::builder()
        .add_service(ProcessorServer::new(server))
        .serve(addr)
        .await
        .unwrap();

    Ok(())
}
