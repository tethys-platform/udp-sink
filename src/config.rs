use serde::Deserialize;

// default DEBUG
fn default_debug() -> bool {
    false
}

// default LOCAL_ADDRESS
fn default_local_address() -> String {
    "0.0.0.0".to_string()
}

// default LOCAL_NETWORK_INTERFACE
fn default_local_network_interface() -> Option<String> {
    None
}

// default SOCKET_SEND_BUFFER_SIZE
fn default_socket_send_buffer_size() -> usize {
    1_024 * 1_024 // 1MB
}

fn default_mtu_size() -> usize {
    7 * 188 
}

#[derive(Clone, Deserialize, Debug)]
pub struct Config {
    // required
    pub port: u16,
    pub hostname: String,
    pub host_port: u16,

    // optional
    #[serde(default = "default_debug")]
    pub debug: bool,
    #[serde(default = "default_local_address")]
    pub local_address: String,
    #[serde(default = "default_local_network_interface")]
    pub local_network_interface: Option<String>,
    #[serde(default = "default_socket_send_buffer_size")]
    pub socket_send_buffer_size: usize,

    #[serde(default = "default_mtu_size")]
    pub mtu_size: usize,
}

pub fn load_config() -> Config {
    match envy::prefixed("TETHYS_PROPERTY_").from_env::<Config>() {
        Ok(config) => {
            debug!("{:#?}", config);
            config
        }
        Err(err) => panic!("{err:#?}"),
    }
}
