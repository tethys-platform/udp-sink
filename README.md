# udp-sink

A Tethys sink processor for sending data to a UDP endpoint. `udp-sink` supports unicast, multicast, IPv4 and IPv6.

## Tethys Config

There are several required and optional configuration properties for running `udp-sink` as a Tethys sink processor.

### Required Tethys Config
---
#### `TETHYS_PROPERTY_HOSTNAME`
`string`

The IP address or hostname of the remote target.

#### `TETHYS_PROPERTY_HOST_PORT`
`integer`

The remote target port.

#### **`TETHYS_PROPERTY_PORT`**
`integer`

The port used by the gRPC server. Outside of development settings, this config is provided by the platform.

#### **`TETHYS_PROPERTY_COMPONENT_ID`**
`string`

The unique ID for this Tethys component. Outside of development settings, this config is provided by the platform. Within development settings, this value needs to be unique within a pipeline.

### Optional Tethys Config
---

#### `TETHYS_PROPERTY_LOCAL_ADDRESS`
`string`

Default: `0.0.0.0`

The local IP address to use when sending packets over multicast. This setting is only used for IPv4 multicast addresses.

> If `LOCAL_NETWORK_INTERFACE` is set, then that value will take precedence over this one. In the event that the interface can't be found, the service will fallback to this setting.

#### `TETHYS_PROPERTY_LOCAL_NETWORK_INTERFACE`
`string`

The local network interface to use when sending packets over multicast. If set, the service will try to find the local address for the interface.

When the multicast address is an IPv4 address and the interface isn't found, the service will fallback to using the `LOCAL_ADDRESS` setting.

When the multicast address is an IPv6 address and this configuration isn't set, or it's set and the interface isn't found, the service will default to use any interface.

Example:

    net1

#### **`TETHYS_DEFAULT_SOCKET_SEND_BUFFER_SIZE`**
`integer`

Default: `1048576` (1MB)

The maximum size of the socket send buffer that should be used. This is a suggestion to the Operating System to indicate how big the socket buffer should be. If this value is set too low, the buffer may fill up before the data can be read, and incoming data will be dropped.

## General Config

#### **`DEBUG`**
`bool`

Default: `false`

Run `udp-sink` in `DEBUG` mode to see debug messages and print out status updates.

#### **`LOG_LEVEL`**
Type: `string`

Default: `INFO`

The service log level, which will filter out any messages below the level.

> If you want to view `DEBUG` messages from the service, you don't need to change this setting -- `$DEBUG` turns that on automatically. However, it also filters out underlying library debug statements. If you want to *also* view those, set `LOG_LEVEL=DEBUG`.

Review [env_logger](https://docs.rs/log/latest/log/enum.Level.html) Rust crate for possible settings.

#### **`LOG_COLORS`**
Type: `string`

Default: `never`

Enable color styling of log messages. By default, colors are disabled. To enable, set the value to `auto` or `always`. Review [env_logger](https://docs.rs/env_logger/0.9.0/env_logger/index.html#disabling-colors) Rust crate for more information.
